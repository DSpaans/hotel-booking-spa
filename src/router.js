import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home';
import About from './views/About';
import Contact from './views/Contact';
import Explore from './views/Explore';
import Book from './views/Book';

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/about',
            name: 'about',
            component: About,
        },
        {
            path: '/contact',
            name: 'contact',
            component: Contact,
        },
        {
            path: '/explore',
            name: 'explore',
            component: Explore,
        },
        {
            path: '/book/:id',
            name: 'book',
            component: Book,
        }
    ]
})
