export default class Room {
    constructor() {
        this.id = 0;
        this.name = '';
        this.image = '';
        this.price = 0;
        this.size = 0;
        this.bed = '';
        this.quantity = 0;
    }

    static fromJSON(json){
        const hotel = new Room();

        hotel.id = json.id;
        hotel.name = json.name;
        hotel.image = json.image;
        hotel.price = json.price;
        hotel.size = json.price;
        hotel.bed = json.bed;
        hotel.quantity = json.quantity;

        return hotel;
    }
}