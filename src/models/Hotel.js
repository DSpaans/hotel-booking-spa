import Room from './Room';

export default class Hotel {
	constructor() {
		this.id = '';
		this.name = '';
		this.description = '';
		this.image = '';
		this.address = '';
		this.rooms = [];
		this.reviews = [];
		this.availableFacilities = [];
		this.stars = 0;
	}

	static fromJSON(json){
		const hotel = new Hotel();

		hotel.id = json.id;
		hotel.name = json.name;
		hotel.description = json.description;
        hotel.image = json.image;
		hotel.address = json.address;
		hotel.rooms = json.rooms.map(Room.fromJSON);
        /*hotel.reviews = json.reviews.map(reviews.fromJSON);
        hotel.availableFacilities = json.availableFacilities.map(availableFacilities.fromJSON);*/
        hotel.stars = json.stars;

		return hotel;
	}
}